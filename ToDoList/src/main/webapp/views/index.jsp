<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
    	String contextPath = request.getContextPath();
    	String message = "This is going to be the to do list: ";
		String title = "To Do List";
    %>
    
    <%!
    public String getDate(){
    	return java.util.Calendar.getInstance().getTime().toString();
    }
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title><%= title %></title>
</head>
<body>
	<%= message %>
	<br>
	<br>
	
	<div class="container">
	  <h2>Choose one of the following: </h2>
	  <div class="list-group">
	    <a href="<%= contextPath %>/views/addActivity.jsp" class="list-group-item list-group-item-action">Add New Actitivy</a>
	    <a href="<%= contextPath %>/views/toDoPending.jsp" class="list-group-item list-group-item-action">Pending To Do List</a>
	    <a href="<%= contextPath %>/views/toDoDone.jsp" class="list-group-item list-group-item-action">Activites Marked as Done</a>
	  </div>
	</div>
	
	<%= getDate() %>
</body>
</html>