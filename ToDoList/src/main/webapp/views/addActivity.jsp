<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <% String title = "Add New Activity"; %>
    <% String contextPath = request.getContextPath(); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title><%= title %></title>
</head>
<body>
	<div class="container">
	  <h2>What's the new Activity?</h2>
	  <form method="post" class="form-inline" action="<%= contextPath %>/ControllerServlet">
	    <label for="email2" class="mb-2 mr-sm-2">Activity Description:</label>
	    <input type="text" class="form-control mb-2 mr-sm-2" name ="activity">
	  <button type="submit" value = "Submit" class="btn btn-primary mb-2">Submit</button>
	  </form>
	</div>
	<div class="container">
	  <div class="list-group">
	    <a href="<%= contextPath %>/views/index.jsp" class="list-group-item list-group-item-action">Volver al men�</a>
	  </div>
	</div>
</body>
</html>