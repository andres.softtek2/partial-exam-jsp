<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <% String title = "Pendings List"; %>
    <% String contextPath = request.getContextPath(); %>
    <%@ page import = "java.util.List" %>
    <%@ page import = "com.softtek.academy.javaweb.dao.ToDoDao" %>
    <%@ page import = "com.softtek.academy.javaweb.beans.ToDoBean" %>
    <% request.setAttribute("list", ToDoDao.getAll()); %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<title><%= title %></title>
</head>
<body>
	
	<p>To Do List: </p>  
	<table border="2" width="70%" cellpadding="2" class="table table-hover">
		
		<thead class="thead-dark">
	    	<tr>
	      	<th scope="col">Activity</th>
	      	<th scope="col">Is Done?</th>
	      	<th scope="col">Click to Mark as Done</th>
	    	</tr>
	  	</thead>
        <c:forEach var="list" items="${list}">
            <tr>
                <td><c:out value="${list.getList()}"/></td>
                <td><c:out value="${list.getIs_done()}"/></td>
                <td><c:out value="Click "/><form action="<%= contextPath %>/ControllerServlet" method="get"><input type="hidden" name="activity" value="${list.getList()}"><button type="submit" value = "Submit" class="btn btn-primary mb-2">HERE</button></form></td>
             </tr>
        </c:forEach>
    </table>
    
    <div class="container">
	  <div class="list-group">
	    <a href="<%= contextPath %>/views/index.jsp" class="list-group-item list-group-item-action">Volver al men�</a>
	  </div>
	</div>  
</body>
</html>