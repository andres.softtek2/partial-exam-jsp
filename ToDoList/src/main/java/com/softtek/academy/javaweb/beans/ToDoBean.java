package com.softtek.academy.javaweb.beans;

import com.softtek.academy.javaweb.dao.ToDoDao;

public class ToDoBean {
	String todo;
	
	public ToDoBean(){
		
	}
	
	public ToDoBean(String todo) {
		super();
		this.todo = todo;
	}

	public String getTodo() {
		return todo;
	}

	public void setTodo(String todo) {
		this.todo = todo;
	}
	
	public void addNew() {
		ToDoDao.addNew(this.todo);
	}
	
}
