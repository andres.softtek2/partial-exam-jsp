package com.softtek.academy.javaweb.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.beans.ToDoBean;
import com.softtek.academy.javaweb.beans.ToDoCompleteBean;


public class DBConnection {
	private static DBConnection conexion;
	Connection conn1;
	
	private DBConnection () {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/sesion3?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
			if (con != null){
				System.out.println("Conneceted to the Data Base!");
			}
		} catch(Exception e) {
			System.out.println(e);
			System.out.println("Aqui hay un error");
		}
		conn1 = con;
	}
	
	public Connection con() {
		return conn1;
	}
	
	public static DBConnection getInstance() {
		if (conexion == null) {
			System.out.println("Estableciando conexi�n...");
			conexion = new DBConnection();
		}
		return conexion;
	}
	
	public List<ToDoCompleteBean> getAll() throws SQLException{
		String query = "SELECT * FROM to_do_list WHERE is_done = 0";
		List<ToDoCompleteBean> result = new ArrayList<ToDoCompleteBean>();
		
		PreparedStatement ps = conn1.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			String list = rs.getString("list");
			Boolean is_done = rs.getBoolean("is_done");
			result.add(new ToDoCompleteBean	(list, is_done));
		}
		
		return result;
	}
	
	public List<ToDoCompleteBean> getAllDone() throws SQLException{
		String query = "SELECT * FROM to_do_list WHERE is_done = 1";
		List<ToDoCompleteBean> result = new ArrayList<ToDoCompleteBean>();
		
		PreparedStatement ps = conn1.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			String list = rs.getString("list");
			Boolean is_done = rs.getBoolean("is_done");
			result.add(new ToDoCompleteBean(list, is_done));
		}
		
		return result;
	}
	
	public void updateActivity(String list) throws SQLException {
		String query = "UPDATE to_do_list SET is_done = 1 WHERE list = '"+ list + "'";
		System.out.println(list);
		
		PreparedStatement ps = conn1.prepareStatement(query);
		ps.executeUpdate();
	}
}
