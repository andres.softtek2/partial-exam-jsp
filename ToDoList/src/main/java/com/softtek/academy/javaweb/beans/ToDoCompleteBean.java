package com.softtek.academy.javaweb.beans;

import java.sql.SQLException;

import com.softtek.academy.javaweb.dao.ToDoDao;

public class ToDoCompleteBean {
	String list;
	Boolean is_done;
	
	public ToDoCompleteBean(String list, Boolean is_done) {
		super();
		this.list = list;
		this.is_done = is_done;
	}

	public ToDoCompleteBean() {	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public Boolean getIs_done() {
		return is_done;
	}

	public void setIs_done(Boolean is_done) {
		this.is_done = is_done;
	}

	public void updateIs_done() throws SQLException {
		ToDoDao.updateIs_done(this.list);
	}
}
