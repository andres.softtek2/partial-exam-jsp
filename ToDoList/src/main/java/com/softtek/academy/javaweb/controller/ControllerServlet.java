package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.ToDoBean;
import com.softtek.academy.javaweb.beans.ToDoCompleteBean;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String activity = request.getParameter("activity");
		ToDoCompleteBean todo = new ToDoCompleteBean();
		todo.setList(activity);
		try {
			todo.updateIs_done();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/views/updateComplete.jsp").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String activity = request.getParameter("activity");
		String message = "The activity has been added succesfully!";
		
		ToDoBean todo = new ToDoBean();
		todo.setTodo(activity);
		todo.addNew();
		response.sendRedirect("/ToDoList/views/activityAdded.jsp?message=" + URLEncoder.encode(message, "UTF-8"));
		
	}

}
