package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.softtek.academy.javaweb.beans.ToDoBean;
import com.softtek.academy.javaweb.beans.ToDoCompleteBean;
import com.softtek.academy.javaweb.services.DBConnection;

public class ToDoDao {
	
	public static void addNew(String todo) {
		DBConnection db = DBConnection.getInstance();
		Connection con = db.con();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO TO_DO_LIST (list) VALUES ('" + todo +"')");
			//ps.setString(1, todo);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
	}
	
	public static List<ToDoCompleteBean> getAll() throws SQLException {
		DBConnection db = DBConnection.getInstance();
		List<ToDoCompleteBean> lista = db.getAll();
		return lista;
	}
	
	public static List<ToDoCompleteBean> getAllDone() throws SQLException{
		DBConnection db = DBConnection.getInstance();
		List<ToDoCompleteBean> lista = db.getAllDone();
		return lista;
	}

	public static void updateIs_done(String list) throws SQLException {
		DBConnection db = DBConnection.getInstance();
		db.updateActivity(list);
	}
}
